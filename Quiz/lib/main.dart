import 'package:flutter/material.dart';

import './quiz.dart';
import './result.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final _questions = const [
    {
      "questionText": "What's your favorite color?",
      "answers": [
        {'text': 'Blue', 'score': 1},
        {'text': 'Red', 'score': 4},
        {'text': 'Green', 'score': 5},
        {'text': 'Black', 'score': 10}
      ]
    },
    {
      "questionText": "What's your favorite animal?",
      "answers": [
        {'text': 'Cat', 'score': 10},
        {'text': 'Dog', 'score': 2},
        {'text': 'Tiger', 'score': 8},
        {'text': 'Lion', 'score': 7}
      ]
    },
    {
      "questionText": "What's your favorite beer?",
      "answers": [
        {'text': 'Coors', 'score': 10},
        {'text': 'Bud Light', 'score': 8},
        {'text': 'Blue Moon', 'score': 1},
        {'text': 'Miller', 'score': 5}
      ]
    },
  ];

  var _questionIndex = 0;
  var _totalScore = 0;

  void _onAnswerQuestion(int score) {
    _totalScore += score;

    setState(() {
      _questionIndex += 1;
    });
  }

  void _resetQuiz() {
    setState(() {
      _questionIndex = 0;
      _totalScore = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("My First App"),
        ),
        body: _questionIndex < _questions.length
            ? Quiz(
                questions: _questions,
                questionIndex: _questionIndex,
                onAnswerQuestion: _onAnswerQuestion,
              )
            : Result(_totalScore, _resetQuiz),
      ),
    );
  }
}
