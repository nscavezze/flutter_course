import 'package:assignment_1/myText.dart';
import 'package:flutter/material.dart';

class TextControl extends StatelessWidget {
  final Function onButtonPress;
  final String text;

  TextControl(this.onButtonPress, this.text);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(10),
      child: Column(
        children: <Widget>[
          Text(
            "What ever you do don't hit the button below",
            style: TextStyle(fontSize: 20),
            textAlign: TextAlign.center,
          ),
          RaisedButton(
            child: Text("Push Me!!!!!"),
            color: Colors.red,
            textColor: Colors.white,
            onPressed: onButtonPress,
          ), MyText(text)
        ],
      ),
    );
  }
}
