import 'package:flutter/material.dart';

class Answer extends StatelessWidget {
  final Function onAnswer;
  final String answerText;

  Answer(this.onAnswer, this.answerText);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: RaisedButton(
        child: Text(answerText),
        onPressed: onAnswer,
        color: Colors.blue,
        textColor: Colors.white,
      ),
    );
  }
}
